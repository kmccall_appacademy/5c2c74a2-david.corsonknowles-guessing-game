# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

# @the_answer = rand(1..100)

# def ask
#   puts "what number would you like to guess this time?"
#   @guess = gets.chomp
#   puts "you chose #{@guess}"
# end

# def check
#
#   if @guess == @the_answer
#     puts "That's the right answer"
#     puts "Your guess was #{@guess} and the answer was #{@the_answer}"
#     puts "You won the game in #{@count} guesses."
#     return true
#   elsif @guess < @the_answer
#     puts "too high"
#   else   #@guess > @the_answer
#     puts "too low"
#   end
#   false
# end


def guessing_game
  count = 0
  the_answer = rand(1..100)
  guess = 0

  until guess == the_answer

    puts "guess a number"
    guess = gets.chomp
    puts "#{ guess }"  # $stdin

    count += 1

    if guess.to_i > the_answer
      puts "too high"

    elsif guess.to_i < the_answer #@guess > @the_answer
      puts "too low"

    else # guess == the_answer
      puts "That's the right answer"
      puts "Your guess was #{ guess } and the answer was #{ the_answer }"
      puts "You won the game in #{ count } guesses."

      print "#{ guess }"
      puts
      print "#{ count }"
      return #guess

    end

  end
end

def make_a_file
  puts "We are going to make a file together"
  puts "What is your new file name?"
  file_name = gets.chomp
  puts "Enter as many lines as you like."
  puts "When a line includes the word 'done' input ends without saving the line"
  File.open(file_name, "w+") do |f|
    f.puts "Here is the first line in my file"
    while true
      entry = gets.chomp
      break if entry.include?("done")
      f.puts entry
    end
    f.puts "Here is the last line of the file I made"
  end
  puts "Congrats you have a new file named #{file_name} "
  puts "Your new file has #{File.readlines(file_name).count} lines"
end

def file_shuffler
  puts "We are going to shuffle your file"
  puts "What's the name of the file you wish to shuffle?"
  file_name = gets.chomp
  f = File.readlines(file_name)
  f.shuffle!
  File.open("#{File.basename(file_name, ".txt")}-shuffled.txt", "w") do |entry|
    f.each do |line|
      entry.puts line
    end
  end
end



if __FILE__ == $PROGRAM_NAME
  # guessing_game
  # make_a_file
  file_shuffler
end
# def guessing_game
#
#   @count = 0
#   until check
#     ask
#     check
#     @count += 1
#   end
#
# end
